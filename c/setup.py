from setuptools import setup, Extension


setup(
    name="fputs",
    version="1.0",
    ext_modules=[
        Extension("fputs", ["module.c"]),
    ]
)
