# Extending Python in C
-----------------------

## Tutorial
-----------
- [Tutorial followed][1]
- [Another interesting source][2]

[1]: https://realpython.com/build-python-c-extension-module/
[2]: https://stackabuse.com/enhancing-python-with-custom-c-extensions/

## Requirements
---------------
```bash
sudo apt install python3-dev
```

## Installation
---------------
Setuptools was used instead of distutils (legacy) as it's more recent:

```bash
pip install .
```

To remove the installed package, run (it works also if the package was installed with `python setup.py install --user`):

```bash
pip uninstall fputs
```

## Call from Python
-------------------

```bash
python
> from fputs import fputs
> fputs("file.txt", "content")
```

## Build with GCC
-----------------
- **Just for tests!:** Compile into an object file (`-c`) not an executable, so it won't complain about missing main() function.

```bash
gcc -c module.c -I/usr/include/python3.6 -o app
```
