#include <Python.h>
#include <stdio.h>

static PyObject *method_fputs(PyObject *self, PyObject *args) {
    // parse arguments passed to python function
    const char *filename, *text;
    if (!PyArg_ParseTuple(args, "ss", &filename, &text))
        return NULL;

    // write text to file & returns < 0 on failure
    FILE *fp = fopen(filename, "w");
    int ret = fputs(text, fp);
    fclose(fp);

    return PyLong_FromLong(ret);
}

// function and module metadata
static PyMethodDef methods_metadata[] = {
    {"fputs", method_fputs, METH_VARARGS, "Docstring of fputs.fputs function"}
};

static PyModuleDef module_metadata = {
    PyModuleDef_HEAD_INIT, "fputs", "Docstring for fputs module", -1, methods_metadata
};

// intialize module & makes it accessible to python interpreter
PyMODINIT_FUNC PyInit_fputs(void) {
    return PyModule_Create(&module_metadata);
}
