# Program
---------
The Cython script (\*.pyx) needs to be compiled first into a C script then into a dynamic library (\*.so) python module locally:

```bash
python setup.py build_ext --inplace
```

or installed directly with other python modules:

```bash
pip install .
```

# Profiling
-----------
Run IPython to profile Fibonacci series scripts written in Python and in Cython.

```bash
In [1]: from fib_python import fib as fib_py
In [2]: from fib_cython import fib as fib_cy

In [3]: %timeit fib_py(10000)
1.82 ms ± 11.3 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)

In [4]: %timeit fib_cy(10000)
7.21 µs ± 55.9 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
```


# CPython implementation
------------------------
1. **Compilation:** Source code (\*.py file) is compiled into *bytecode* (\*.pyc file).
2. **Interpretation:** *Bytecode* instructions are executed by the *virtual machine*.

## Comments
- Interpreted languages are not translated into machine code beforehand (i.e. no explicit compilation step like in C).
- C++ is faster than Python because it's **statically-typed**.
- Python bytecode is cross-platform.
- Python threads cannot run on different cores concurrently, due to the Global Interpreter Lock (GIL).
- Pybind11 can be used to write Python wrappers in C++ (Python types are supported).
