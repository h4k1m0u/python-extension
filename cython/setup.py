from setuptools import setup
from Cython.Build import cythonize


setup(
    name="fib_cython",
    version="1.0",
    ext_modules=cythonize("fib_cython.pyx"),
)
