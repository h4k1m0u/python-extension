#cython: language_level=3
def fib(int n):
    """
    Only Python functions (not cdef) can be exported to Python.
    """
    cdef int a = 0
    cdef int b = 1
    cdef int i

    for i in xrange(n):
        a, b = b, a + b

    return a
